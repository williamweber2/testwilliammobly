<?php

class Product extends Eloquent
{
	protected $fillable = array("category_id", "title", "description", "available", "image");

	public static $rules = array(
		"category_id" => "required|array",
		"title" => "required|min:2",
		"description" => "required|min:20",
		"price" => "required|numeric",
		"available" => "integer",
		"image" => "required|image|mimes:jpeg, jpg, bmp, png, gif"
	);

	public function category()
	{
		return $this->hasMany("ProductCategory");
	}
}