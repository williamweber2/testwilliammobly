<?php
class DatabaseCart extends Eloquent
{
	protected $table = 'cart';

	public function user()
	{
		return $this->belongsTo("User");
	}
}