<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('products', function($table){
			$table->increments("id");
			$table->string('title');
			$table->text('description');
			$table->text('characteristics');
			$table->decimal('price', 6, 2);
			$table->boolean('available')->default(1);
			$table->string('image');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return vpopmail_del_domain(domain)
	 */
	public function down()
	{
		Schema::drop('products');
	}

}