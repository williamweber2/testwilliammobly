<?php

class UserTableSeeder extends Seeder
{
	public function run()
	{
		$user = new User();
		$user->name = 'Administrador';
		$user->lastname = 'Teste';
		$user->email = 'admin@teste.com.br';
		$user->password = Hash::make('moblycontrataeu');
		$user->phone = '333444555';
		$user->admin = 1;
		$user->save();	
	}
}