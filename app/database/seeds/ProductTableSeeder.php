<?php

class ProductTableSeeder extends Seeder
{
    public function run()
    {
        $salaCategory = new Category();
        $salaCategory->name = "Sala";
        $salaCategory->save();

        $cozinhaCategory = new Category();
        $cozinhaCategory->name = "Cozinha";
        $cozinhaCategory->save();

        $banheiroCategory = new Category();
        $banheiroCategory->name = "Banheiro";
        $banheiroCategory->save();

        $quartoCategory = new Category();
        $quartoCategory->name = "Quarto";
        $quartoCategory->save();

        $product = new Product();
        $product->title = "Mesa Retangular 387";
        $product->description = "Mesa Retangular 387 Cromada & Vidro Incolor Carraro";
        $product->characteristics = "Altura: 78 cm; Largura 140 cm; Profundidade 80 cm;
Peso 23,300 kg;";
        $product->price = 99.99;
        $product->available = 1;
        $product->image = "img/products/mesa.jpg";
        $product->save();

        $productCategory = new ProductCategory();
        $productCategory->category_id = $salaCategory->id;
        $productCategory->product_id = $product->id;
        $productCategory->save();

        $productCategory = new ProductCategory();
        $productCategory->category_id = $cozinhaCategory->id;
        $productCategory->product_id = $product->id;
        $productCategory->save();

        $product = new Product();
        $product->title = "Torneira Prateada";
        $product->description = "Torneira Prateada em grande para qualquer pia";
        $product->characteristics = "Altura: 78 cm; Largura 140 cm; Profundidade 80 cm;
Peso 23,300 kg;";
        $product->price = 20.99;
        $product->available = 1;
        $product->image = "img/products/torneira.jpg";
        $product->save();

        $productCategory = new ProductCategory();
        $productCategory->category_id = $banheiroCategory->id;
        $productCategory->product_id = $product->id;
        $productCategory->save();

        $productCategory = new ProductCategory();
        $productCategory->category_id = $cozinhaCategory->id;
        $productCategory->product_id = $product->id;
        $productCategory->save();

        $product = new Product();
        $product->title = "Cama Kingsize";
        $product->description = "Cama kingsize em madeira. Acompanha colchão.";
        $product->characteristics = "Altura: 78 cm; Largura 140 cm; Profundidade 80 cm;
Peso 23,300 kg;";
        $product->price = 200.99;
        $product->available = 1;
        $product->image = "img/products/cama.jpg";
        $product->save();

        $productCategory = new ProductCategory();
        $productCategory->category_id = $quartoCategory->id;
        $productCategory->product_id = $product->id;
        $productCategory->save();
    }
}
