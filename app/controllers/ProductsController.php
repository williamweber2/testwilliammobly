<?php

class ProductsController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->beforeFilter('admin');
	}

	public function getIndex()
	{

		$categories = array();

		foreach (Category::all() as $category) {
			$categories[$category->id] = $category->name;
		}

		return View::make("products.index")
			->with('products', Product::all())
			->with('categories', $categories);


	}

	public function postCreate()
	{
		$validator = Validator::make(Input::all(), Product::$rules);



		if ($validator->passes()) {
			$product = new Product();
			$product->title = Input::get('title');
			$product->description = Input::get('description');
			$product->price = Input::get('price');

			$image = Input::file('image');
			$filename = date("YmdHis")."-".$image->getClientOriginalName();
			Image::make($image->getRealPath())->resize(300, 300)->save('public/img/products/'.$filename);

			$product->image = 'img/products/'.$filename;
			$product->save();

			$categories = Input::get('category_id');
			foreach($categories as $category)
			{
				$productsCategories = new ProductCategory();
				$productsCategories->product_id = $product->id;
				$productsCategories->category_id = $category;
				$productsCategories->save();
			}

			return Redirect::to("admin/products/index")
				->with("message", "Produto criado com sucesso");
		}

		return Redirect::to("admin/products/index")
			->with("message", "Erro ao criar o produto!")
			->withErrors($validator)
			->withInput();
	}

	public function postDestroy()
	{
		$product = Product::find(Input::get('id'));

		if ($product) {
			File::delete('public/'.$product->image);
			$productsCategories = ProductCategory::where('product_id', '=', $product->id)->get();
			foreach ($productsCategories as $productCategory) {
				$productCategory->delete();
			}
			$product->delete();
			return Redirect::to("admin/products/index")
				->with("message", "Produto removido com sucesso!");
		}

		return Redirect::to("admin/products/index")
			->with("message", "Erro ao remover o produto!");
	}

	public function postToggleAvailable()
	{
		$product = Product::find(Input::get('id'));

		if ($product) {
			$product->available = Input::get('available');
			$product->save();
			return Redirect::to("admin/products/index")
				->with('message', 'Produto atualizado com sucesso!');
		}

		return Redirect::to("admin/products/index")
			->with('message', 'Erro ao atualiza o produto.');
	}
}