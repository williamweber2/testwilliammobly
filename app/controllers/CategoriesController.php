<?php

class CategoriesController extends BaseController
{
	public function __construct()
	{
		parent::__construct();
		$this->beforeFilter('csrf', array('on'=>'post'));
		$this->beforeFilter('admin');
	}

	public function getIndex()
	{
		return View::make("categories.index")
			->with('categories', Category::all());
	}

	public function postCreate()
	{
		$validator = Validator::make(Input::all(), Category::$rules);

		if (!$validator->fails()) {
			$category = new Category();
			$category->name = Input::get("name");
			$category->save();

			return Redirect::to("admin/categories/index")
				->with("message", "Categoria criada com sucesso");
		}

		return Redirect::to("admin/categories/index")
			->with("message", "Erro ao criar a categoria!")
			->withErrors($validator)
			->withInput();
	}

	public function postDestroy()
	{
		$category = Category::find(Input::get('id'));

		if ($category) {
			$category->delete();
			return Redirect::to("admin/categories/index")
				->with("message", "Categoria removida com sucesso!");
		}

		return Redirect::to("admin/categories/index")
			->with("message", "Erro ao remover a categoria!");
	}
}