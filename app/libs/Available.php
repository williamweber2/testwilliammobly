<?php

class Available
{
	public static function display($available) {
		if ($available == 0) {
			echo "Sem estoque";
		} elseif($available == 1) {
			echo "Disponivel em estoque";
		}
	}

	public static function displayClass($available) {
		if ($available == 0) {
			echo "out-stock";
		} elseif($available == 1) {
			echo "in-stock";
		}
	}
}