@extends('layouts.main')

@section('search-keyword')

	<div class="search">
		<h2>Resultados de busca para: {{ $keyword }}</h2>
	</div>

@stop

@section('content')

	<div class="search-result">
		@foreach($products as $product)
		<div class="product">
			<a href="/store/view/{{ $product->id }}">
				{{ HTML::image($product->image, $product->title, array('width'=>'200', 'height'=>'200')) }}
			</a>
			<h3>
				<a href="/store/view/{{ $product->id }}">{{ $product->title }}</a>
			</h3>
			<p>
				{{ $product->description }}
			</p>
			<h5>
				Disponibilidade: 
				<span class="{{ Available::displayClass($product->available) }}">
					{{ Available::display($product->available) }}
				</span>
			</h5>
			<p>
				<span class="price"> $ {{ $product->price }}</span>
				{{ Form::open(array('url'=>'store/addtocart')) }}
				{{ Form::hidden('quantity', 1) }}
				{{ Form::hidden('id', $product->id) }}
				{{ Form::submit('Adicionar ao carrinho') }}
				{{ Form::close() }}
			</p>
		</div>
		@endforeach
	</div>

@stop