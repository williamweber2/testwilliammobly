@extends('layouts.main')

@section('content')
<hr />
	<h2>
		{{ $category->name }}
	</h2>
	<div class="lista-produtos">
		@foreach($products as $product)
		<div class="product" style="float:left;">
			<a href="/store/view/{{ $product->id }}">
				{{ HTML::image($product->image, $product->title, array('width'=>'200', 'height'=>'200')) }}
			</a>
			<h3>
				<a href="/store/view/{{ $product->id }}">{{ $product->title }}</a>
			</h3>
			<p>
				{{ $product->description }}
			</p>
			<h5>
				Disponibilidade: 
				<span class="{{ Available::displayClass($product->available) }}">
					{{ Available::display($product->available) }}
				</span>
			</h5>
			<p>
				<span class="price"> $ {{ $product->price }}</span>
				{{ Form::open(array('url'=>'store/addtocart')) }}
				{{ Form::hidden('quantity', 1) }}
				{{ Form::hidden('id', $product->id) }}
				{{ Form::submit('Adicionar ao carrinho') }}
				{{ Form::close() }}
			</p>
		</div>
		@endforeach
	</div>
	<div style='clear:both;'></div>
<hr />

@stop

@section('pagination')

	<div class="pagination">
		{{ $products->links() }}
	</div>

@stop