@extends('layouts.main')

@section('content')

	<table border="1">
		<tr>
			<th>ID</th>
			<th>Produto</th>
			<th>Valor</th>
			<th>Quantidade</th>
			<th>Total</th>
		</tr>
		@foreach($products as $product)
			<tr>
				<td>{{ $product->id }}</td>
				<td>{{ HTML::image($product->image, $product->name, array("width"=>"50", "height"=>"50")) }} - {{ $product->name }}</td>
				<td>{{ $product->price }}</td>
				<td>{{ $product->quantity }}</td>
				<td>
					{{ $product->price }} - 
					<a href="/store/removeitem/{{ $product->identifier }}">
						Remover Produto
					</a>
				</td>
			</tr>
		@endforeach
		<tr>
			<td colspan="5">
				Total: {{ Cart::total() }}
			</td>
		</tr>
	</table>
	<hr />
	{{ HTML::link('/store/pay', 'Efetuar pagamento') }}
	<hr />
	{{ HTML::link('/', 'Continuar comprando') }}
@stop