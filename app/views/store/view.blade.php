@extends('layouts.main')

@section('content')

	<div class="product-detail">
		<div class="product-image">
			{{ HTML::image($product->image, $product->title) }}
		</div>
		<div class="product-details">
			<h1>
				{{ $product->title }}
			</h1>
			<p>
				{{ $product->description }}
			</p>
			<hr />
		</div>
		<div class="product-info">
			<p class="price">
				R$ {{ $product->price }}
			</p>
			<p>
				Disponibilidade:
				<span class="{{ Available::displayClass($product->available) }}">
					{{ Available::display($product->available) }}
				</span>
			</p>
			<p>
				Id: {{ $product->id }}
			</p>
		</div>
		<p>
				<span class="price"> $ {{ $product->price }}</span>
				{{ Form::open(array('url'=>'store/addtocart')) }}
				{{ Form::label('quantity', 'Quantidade') }}
				{{ Form::text('quantity', 1, array('maxlength'=>2)) }}
				{{ Form::hidden('id', $product->id) }}
				{{ Form::submit('Adicionar ao carrinho') }}
				{{ Form::close() }}
			</p>
	</div>

@stop