@extends("layouts.main")

@section('content')

	<div id="admin">
		<h1> Produtos Admin</h1>

		<hr />

		<h2>Produtos</h2>

		<ul>
			@foreach($products as $product)
				<li>
					{{ HTML::image($product->image, $product->title, array('width'=>50)) }}
					{{ $product->title }} - 
					{{ Form::open(array('url'=>'admin/products/destroy')) }}
					{{ Form::hidden('id', $product->id) }}
					{{ Form::submit('remover') }}
					{{ Form::close() }} -
					{{ Form::open(array('url'=> 'admin/products/toggle-available')) }}
					{{ Form::hidden('id', $product->id) }}
					{{ Form::select('available', array('0'=>'sem estoque', '1'=>'com estoque'), $product->available) }}
					{{ Form::submit('atualizar') }}
					{{ Form::close() }}
				</li>
			@endforeach
		</ul>

		<hr />

		<h2>Criar novo produto</h2>

		@if($errors->has())
		<hr />
			<div class="error">
				<p>Erros detectados:</p>
				<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				</ul>
			</div>
			<hr />
		@endif

		<hr />

		{{ Form::open(array('url'=>'admin/products/create', 'files'=>true)) }}
		<p>
			{{ Form::label('category_id', 'Categoria') }}
			{{ Form::select('category_id[]', $categories, null, array('multiple'=>true) ) }}
		</p>
		<p>
			{{ Form::label('title') }}
			{{ Form::text('title') }}
		</p>
		<p>
			{{ Form::label('description') }}
			{{ Form::text('description') }}
		</p>
		<p>
			{{ Form::label('price') }}
			{{ Form::text('price', null) }}
		</p>
		<p>
			{{ Form::label('image', 'Escolha uma imagem') }}
			{{ Form::file('image') }}
		</p>
		{{ Form::submit('Criar produto.') }}
		{{ Form::close() }}
	</div>


@stop