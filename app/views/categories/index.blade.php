@extends("layouts.main")

@section('content')

	<div id="admin">
		<h1> Categorias Admin</h1>

		<hr />

		<h2>Categorias</h2>

		<ul>
			@foreach($categories as $category)
				<li>
					{{ $category->name }} - 
					{{ Form::open(array('url'=>'admin/categories/destroy')) }}
					{{ Form::hidden('id', $category->id) }}
					{{ Form::submit('remover') }}
					{{ Form::close() }}
				</li>
			@endforeach
		</ul>

		<h2>Criar nova categoria</h2>

		@if($errors->has())
			<div class="error">
				<p>Erros detectados:</p>
				<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				</ul>
			</div>
		@endif

		{{ Form::open(array('url'=>'admin/categories/create')) }}
		<p>
			{{ Form::label('name') }}
			{{ Form::text('name') }}
		</p>
		{{ Form::submit('Criar categoria.') }}
		{{ Form::close() }}
	</div>


@stop