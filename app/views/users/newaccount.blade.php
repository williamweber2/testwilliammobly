@extends('layouts.main')

@section('content')

	<div class="newaccount">
		<h1>Criar uma nova conta</h1>

		@if($errors->has())
			<div class="error">
				<p>Erros detectados:</p>
				<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error }}</li>
				@endforeach
				</ul>
			</div>
		@endif

		{{ Form::open(array('url'=>'users/create')) }}
		<p>
			{{ Form::label('Nome') }}
			{{ Form::text('name') }}
		</p>
		<p>
			{{ Form::label('Sobrenome') }}
			{{ Form::text('lastname') }}
		</p>
		<p>
			{{ Form::label('Email') }}
			{{ Form::text('email') }}
		</p>
		<p>
			{{ Form::label('telefone') }}
			{{ Form::text('phone') }}
		</p>
		<p>
			{{ Form::label('Endereço entrega') }}
			{{ Form::text('deliveryaddress') }}
		</p>
		<p>
			{{ Form::label('Endereço cobrança') }}
			{{ Form::text('billingaddress') }}
		</p>
		<p>
			{{ Form::label('Senha') }}
			{{ Form::password('password') }}
		</p>
		<p>
			{{ Form::label('Confirme a senha') }}
			{{ Form::password('password_confirmation') }}
		</p>
		{{ Form::submit("Criar novo usuario.") }}
		{{ Form::close() }}
	</div>

@stop