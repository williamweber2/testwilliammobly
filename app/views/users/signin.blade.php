@extends('layouts.main')

@section('content')

	<div class="signin">
		<h1>Login</h1>
		{{ Form::open(array('url'=>'users/signin')) }}
		<p>
			{{ Form::label('email') }}
			{{ Form::text('email') }}
		</p>
		<p>
			{{ Form::label('password') }}
			{{ Form::password('password') }}
		</p>
		{{ Form::submit('Login') }}
		{{ Form::close() }}
	</div>
	<div class="signup">
		{{ HTML::link('users/newaccount', 'Cadastrar-se') }}
	</div>

@stop