	<html>
	<head>
		<title>Carrinho teste William</title>
	</head>
	<body>
		<div class="logotipo">
			<a href="/">Teste William Mobly</a>
		</div>

		<hr />

		<div class="user-credential" style='border:1px; background-color:#e1e1e1;'>
			@if(Auth::check())
				<ul>
					<li>
						<a href="#">{{ Auth::user()->name }}</a>
						<ul>
							<li><a href="store/cart">Carrinho</a></li>
							@if(Auth::user()->admin == 1)
								<li>{{ HTML::link('admin/categories', 'Gerenciar Categorias') }}</li>
								<li>{{ HTML::link('admin/products', 'Gerenciar Produtos') }}</li>
							@endif

							<li>{{ HTML::link('users/signout', 'Logout') }}</li>
						</ul>
					</li>
				</ul>
			@else
				<ul>
					<li>
						<a href="#">Visitante</a>
						<ul>
							<li>{{ HTML::link('users/signin', 'Login') }}</li>
							<li>{{ HTML::link('users/newaccount', 'Cadastre-se') }}</li>
						</ul>
					</li>
				</ul>
			@endif 
		</div>

		<hr />
		
		@if (Session::has('message'))
			<p>{{ Session::get('message') }}</p>
		@endif

		<h3>Navegue por categorias</h3>
		<ul>
			@foreach($catnav as $cat)
				<li>
					{{ HTML::link('/store/category/'.$cat->id, $cat->name) }}
				</li>
			@endforeach
		</ul>

		<hr />

		<h3>Ferramenta de busca</h3>
		<div class="search-form">
			{{ Form::open(array('url'=>'/store/search', 'method'=>'get')) }}
			{{ Form::text('keyword', null, array('placeholder'=>'Insira a sua busca...')) }}
			{{ Form::submit('Pesquisar') }}
			{{ Form::close() }}
		</div>

		<hr />

		@yield('search-keyword')

		<hr />

		@yield('content')

		<hr />

		@yield('pagination')
	</body>
</html>
